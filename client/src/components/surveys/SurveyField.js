import React from 'react';

const SurveyField = ({input, label, meta: {error, touched}}) => {
  return (
    <div>
      <label>{label}</label>
      <input {...input} style={{marginBottom: '0.5rem'}}/>
      <div className="red-text" style={{marginBottom: '2rem'}}>
        {touched && error}
      </div>
    </div>
  );
};

export default SurveyField;
