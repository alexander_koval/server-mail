import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { submitSurvey } from '../../actions';
import formFields from './formFields';

const SurveyFormReview = ({onCancel, inputValues, submitSurvey, history}) => {
  const reviewFields = _.map(formFields, ({name, label}) => {
    return(
      <div key={name}>
        <label>{label}</label>
        <div>{inputValues[name]}</div>
      </div>
    );
  });
  return (
    <div>
      <h5>Please confirm your entries</h5>
      {reviewFields}
      <button className="orange darken-3 btn-flat white-text" onClick={onCancel}>
        Back
        <i className="left material-icons">chevron_left</i>
      </button>
      <button
        className="green btn-flat right white-text"
        onClick={() => submitSurvey(inputValues, history)}
      >
        Send survey
        <i className="right material-icons">send</i>
      </button>
    </div>
  );
};

function mapStateToProps(state) {
  return {inputValues: state.form.surveyForm.values};
}

export default connect(mapStateToProps, {submitSurvey})(withRouter(SurveyFormReview));
